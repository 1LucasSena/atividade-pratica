# TerraLAB


## Atividade Prática - Sprint 1

##### Lucas Sena


## Questões Teóricas

> **1. O que é Git?** \
>  O Git é uma ferramenta poderosa e versátil de controle de versão, adequada para diversas aplicações. É gratuito, de código aberto e amigável, sendo uma escolha excelente para desenvolvedores, gerentes de projetos e acadêmicos.



> **2. O que é a staging area?** \
> É um local temporário onde os desenvolvedores podem colocar as alterações que desejam adicionar ao próximo commit.

> **3. O que é o working directory?** \
> Working directory é o diretório onde os desenvolvedores trabalham em seus arquivos e adicionam alterações ao repositório Git. É o ponto de partida para todas as operações do Git.

> **4. O que é um commit?** \
> Um commit é uma unidade de alteração que é armazenada no histórico de um projeto. Os commits permitem aos desenvolvedores rastrear as alterações feitas em um projeto ao longo do tempo.

> **5. O que é uma branch?** \
> Uma branch é uma cópia do histórico de um projeto. As branches permitem aos desenvolvedores trabalhar em diferentes partes de um projeto ao mesmo tempo.

> **6. O que é o head no Git?** \
> O head é um ponteiro para o commit mais recente em uma branch. Ele permite aos desenvolvedores manter o controle do que está sendo trabalhado.



> **7. O que é um merge?** \
> O "merge" é o processo de combinar os commits de duas branches diferentes, integrando as modificações de ambas as ramificações em uma única branch.

> **8. Explique os 4 estados de um arquivo no Git.** \
>  Um arquivo do Git pode ter esses 4 estados a seguir:
> - Untracked: Arquivos que não fazem parte do repositório Git são aqueles que ainda não foram adicionados. O Git não rastreia as alterações feitas nesses arquivos. Para adicionar um arquivo untracked ao repositório, use o comando git add.
> - Committed: Arquivos que foram commitados estão no histórico do repositório Git. As alterações feitas nesses arquivos foram salvas permanentemente. Para commitar um arquivo staged, use o comando git commit.
> - Modified: Arquivos que foram modificados desde o último commit são aqueles que tiveram seu conteúdo alterado, mas ainda não foram adicionados à staging area. O Git rastreia as alterações feitas nesses arquivos, mas elas ainda não foram commitadas. Para ver quais arquivos estão no estado modified, use o comando git status.
> Staged: Arquivos que foram adicionados à staging area estão prontos para serem commitados. O Git está preparado para salvar as alterações feitas nesses arquivos. Para adicionar um arquivo modified à staging area, use o comando git add.

> **9. Explique o comando git init.** \
>  O git init é o ponto de partida para a criação de um novo repositório Git, permitindo que você inicie o controle de versão e o acompanhamento das mudanças em seus projetos.

> **10. Explique o comando git add.** \
> O comando git add é usado para adicionar arquivos ou diretórios à staging area do Git.

> **11. Explique o comando git status.** \
> O comando git status é usado para verificar o estado do seu repositório Git. Ele mostra quais arquivos foram modificados, quais estão preparados para commit e quais precisam ser adicionados à staging area.

> **12. Explique o comando git commit.** \
> O comando git commit é uma ferramenta essencial para o controle de versão com o Git. Ao usá-lo corretamente, você pode garantir que seu código seja bem organizado e que você possa facilmente reverter alterações se necessário.

> **13. Explique o comando git log.** \
> O comando git log é uma ferramenta essencial para o controle de versão com o Git. Ao usá-lo regularmente, você pode acompanhar as alterações feitas no seu projeto, identificar problemas e reverter alterações se necessário.


> **14. Explique o comando git checkout -b.** \
> O git checkout -b é uma maneira conveniente de criar e alternar para uma nova branch em um único passo.

> **15. Explique o comando git reset e suas três opções.** \
> O comando git reset tem três opções:
> - soft-> A opção soft desfaz o último commit, mas mantém as alterações na staging area. Isso significa que as alterações ainda estão presentes nos arquivos, mas não estão mais marcadas para serem commitadas. Para commitar as alterações novamente, use o comando git add seguido do comando git commit.
> - hard-> A opção hard desfaz o último commit, stages e alterações nos arquivos. Isso significa que as alterações são removidas da staging area e dos arquivos.
> - mixed-> A opção mixed desfaz o último commit e stages. Isso significa que as alterações são removidas da staging area, mas ainda estão presentes nos arquivos. Para remover as alterações dos arquivos, use o comando git checkout -- <arquivo> para cada arquivo afetado.

> **16. Explique o comando git revert.** \
> O comando git revert é usado para desfazer alterações feitas em um commit anterior.

> **17. Explique o comando git clone.** \
> O comando git clone é usado para criar uma cópia de um repositório Git existente.

> **18. Explique o comando git push.** \
> O comando git push é usado para enviar (ou push) as alterações feitas no seu repositório Git local para um repositório Git remoto

> **19. Explique o comando git pull.** \
> O comando git pull é usado para buscar e baixar alterações feitas por outras pessoas no repositório remoto e imediatamente atualizar o repositório local para corresponder a essas alterações.

> **20. Como ignorar o versionamento de arquivos no Git?** \
> Para ignorar o versionamento de arquivos no Git, você pode criar um arquivo chamado .gitignore no diretório raiz do seu repositório. Este arquivo contém uma lista de padrões de arquivos ou diretórios que você deseja ignorar.

> **21. No terralab utilizamos as branches master ou main, develop e staging. Explique o objetivo de cada uma.** \
> - master-> É a branch principal do projeto, que contém o código da versão atual do projeto que está em ação no mercado. É a branch mais estável e deve ser sempre atualizada com as alterações mais recentes.
> - develop->  É uma branch ramificada a partir da master, que serve como área para novas modificações e melhorias. É onde os desenvolvedores trabalham em novas funcionalidades e correções de bugs.
> - staging->  É uma branch ramificada a partir da develop, que serve para testar as alterações feitas na develop. É uma cópia da develop que é usada para executar testes automatizados e manuais.

 ## Questões Práticas


> **1. A essa altura, você já deve ter criado a sua conta do GitLab, não é? Crie um repositório público na sua conta, que vai se chamar Atividade Prática e por fim sincronize esse repositório em sua máquina local.** \

**2. Dentro do seu reposotorio, crie um arquivo chamado README.md e leia o [artigo](https://raullesteves.medium.com/github-como-fazer-um-readme-md-bonit%C3%A3o-c85c8f154f8) como fazer um readme.md bonitão e deixe esse README.md abaixo bem bonitão:**
> - README.md onde o trainne irá continuamente responder as perguntas em formas de commit.
> - Inserção de código, exemplo de commit de feature:
>   - *git commit -m "{PERGUNTA}" -m "{RESPOSTA}"*

> **3. Crie nesse repositório um arquivo que vai se chamar calculadora.js, abra esse arquivo em seu editor de códigos favoritos e adicione o seguinte código:**
> ```js
>const args = process.argv.slice(2);
>console.log(parseInt(args[0]) + parseInt(args[1]));
> ```
> **Descubra o que esse código faz através de pesquisas na internet, também descubra como executar um código em javascript e dado que o nome do nosso arquivo é calculadora.js e você entendeu o que o código faz, escreva abaixo como executar esse código em seu terminal:** \
> Para executa-lo deve utilizar o comando a seguir: \
> `node calculadora.js a b`
> 
> O código recebe dois números, "a" e "b", a partir de um array de argumentos e imprime a soma deles na saída padrão.

**4. Agora que você já tem um código feito e a resposta aqui, você precisa subir isso para seu repositório. Sem usar git add . descubra como adicionar apenas um arquivo ao seu histórico de commit e adicione calculadora.js a ele.** \
> Para subir devemos utilizar o comando a seguir: \
> `git add calculadora.js`

**Que tipo de commit esse código deve ter de acordo ao conventional commit.** \
> feat: MENSAGEM
> 
> **Que tipo de commit o seu README.md deve contar de acordo ao conventional commit.** \
> docs: MENSAGEM

**Por fim, faça um push desse commit.**

> **5. Copie e cole o código abaixo em sua calculadora.js:**
> ```js
>const soma = () => {
>   console.log(parseInt(args[0]) + parseInt(args[1]));
>};
> 
>const args = process.argv.slice(2);
> 
>soma();
> ```
> **Descubra o que essa mudança representa em relação ao conventional commit e faça o devido commit dessa mudança.** \
>refactor: MENSAGEM

> **6. João entrou em seu repositório e o deixou da seguinte maneira:**
> ```js
>const soma = () => {
>    console.log(parseInt(args[0]) + parseInt(args[1]));
>};
>
>const sub = () => {
>    console.log(parseInt(args[0]) - parseInt(args[1]));  
>}
>
>const args = process.argv.slice(2);
>
>switch (args[0]) {
>    case 'soma':
>        soma();
>    break;
>
>    case 'sub':
>        sub();
>    break;
>    
>    default:
>        console.log('does not support', args[0]);
>}
> ```
> **Depois disso, realizou um git add . e um commit com a mensagem: "Feature: added subtraction"**
>
> **Faça como ele e descubra como executar o seu novo código.** \
> `node calculadora.js operacao a b`

> **Nesse código, temos um pequeno erro, encontre-o e corrija para que a soma e <s>divisão</s> subtração funcionem.** \
>Os erros identificados eram:

1. Nas funções `soma()` e `sub()`, os números estavam nos índices errados do array `args`. Estavam como `parseInt(args[0]) + parseInt(args[1])`, mas deveriam ser `parseInt(args[1]) + parseInt(args[2])`, uma vez que `args[0]` contém a operação.
2. No bloco `default` do switch, o array estava escrito como `arg[0]`, mas deveria ser `args[0]`.

> **Por fim, commit sua mudança.** \
>erro de subtraçao corrigido
> **7. Por causa de joãozinho, você foi obrigado a fazer correções na sua branch principal! O produto foi pro saco e a empresa perdeu muito dinheiro porque não conseguiu fazer as suas contas, graças a isso o seu chefe ficou bem bravo e mandou você dar um jeito disso nunca acontecer.**
>
> **Aprenda a criar uma branch, e desenvolva a feature de divisão nessa branch.** \
> Para criar uma nova branch, você deve usar o comando:
> `git checkout -b NOME_BRANCH`\
>O nome utilizado foi desenvolvimento\

> **8. Agora que a sua divisão está funcionando e você garantiu que não afetou as outras funções, você está apto a fazer um merge request.** \
> **Em seu gitlab, descubra como realizá-lo de acordo com o gitflow.** \
>Para criar uma nova branch de staging a partir da branch de desenvolvimento, testar o código nessa nova branch e, em seguida, mesclá-la com a branch principal (main)

> **9. João quis se redimir dos pecados e fez uma melhoria em seu código, mas ainda assim, continuou fazendo um push na master, fez a seguinte alteração no código e fez o commit com a mensagem:** \
> `feat: add conditional evaluation`
> ```js
>var x = args[0];
>var y = args[2];
>var operator = args[1];
>
>function evaluate(param1, param2, operator) {
>  return eval(param1 + operator + param2);
>}
>
>if ( console.log( evaluate(x, y, operator) ) ) {}
>```
> **Para piorar a situação, joão não te contou como executar esse novo código, enquanto você não descobre como executá-lo lendo o código, e seu chefe não descobriu que tudo está comprometido, faça um revert através do seu gitlab para que o produto volte ao normal o quanto antes!**
>Criar uma nova branch chamada "desenvolvimento" usando o comando:\
>`git checkout -b desenvolvimento`
> visualizar o histórico de commits e identificou um commit específico com o hash:\
>`c3a1c3112bc89f1ec6f1d82a486d9e403e3fb42a`.\
> reverter esse commit usando o comando:\
>`git revert c3a1c3112bc89f1ec6f1d82a486d9e403e3fb42a`
>a mensagem de commit resultante foi:\
>`"Revert 'feat: add conditional evaluation'"` 

**10. Descubra como executar esse novo código e que operações ele é capaz de realizar. Deixe sua resposta aqui, e explique o que essas funções javascript fazem.** \

>O código de Joãozinho utiliza a função `eval()`, que recebe uma string como parâmetro e, se essa string for uma expressão aritmética válida, a função a avalia e retorna o resultado da expressão. No entanto, ao executar esse código, é importante usar os símbolos dos operadores em vez de seus nomes. Por exemplo, em vez de usar "soma", "sub", "div", etc., você deve usar os símbolos aritméticos, como "+", "-", "/", e assim por diante.\

>Portanto, para executar o código corretamente, você deve fazer o seguinte:

`node calculadora.js a operacao b`

>Onde "a" e "b" representam números e "operacao" é o símbolo do operador aritmético desejado, como "+", "-", "/", e assim por diante. Isso permitirá que a função `eval()` avalie corretamente a expressão aritmética e retorne o resultado esperado.
